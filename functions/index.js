const functions = require("firebase-functions");
const admin = require('firebase-admin')

const serviceAccount = require("./service-account-key.json")

admin.initializeApp({
    credential: admin.credential.cert(serviceAccount),
});

const db = admin.firestore();

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//

exports.createCustomToken = functions.https.onCall((uid, contest) => {
    return admin
        .auth()
        .createCustomToken(uid)
        .then((customToken) => {
            return customToken;
        })
        .catch((error) => {
            console.log('Error creating custom token', error);
        });
})


exports.verifyEditorRole = functions.https.onCall((req, res) => {
    const payLoad = req
    return db.collection('userProfiles').doc(payLoad.uid).get().then(
        ds => {
            if ((ds.data().roles).includes("editor"))
                return { status: true, redirectUrl: `${payLoad.openUrl}/home?customToken=${payLoad.customToken}&sessionCookies=${payLoad.session}` }
            return { status: true, message: "UNAUTHORIZED REQUEST!", redirectUrl: `${payLoad.openUrl}/login` }
        })
})

exports.getUserRedirection = functions.https.onCall((req, res) => {
    const payLoad = req
    return db.collection('userProfiles').doc(payLoad.uid).get().then(
        ds => {
            if ((ds.data().roles).includes("user"))
                return { status: true, redirectUrl: `${payLoad.openUrl}/home?customToken=${payLoad.customToken}&sessionCookies=${payLoad.session}` }
            return { status: true, message: "UNAUTHORIZED REQUEST!", redirectUrl: `${payLoad.openUrl}/login` }
        })
})


exports.getAdminRedirection = functions.https.onCall((req, res) => {
    const payLoad = req
    return db.collection('userProfiles').doc(payLoad.uid).get().then(
        ds => {
            if ((ds.data().roles).includes("admin"))
                return { status: true, redirectUrl: `${payLoad.openUrl}/home?customToken=${payLoad.customToken}&sessionCookies=${payLoad.session}` }
            return { status: true, message: "UNAUTHORIZED REQUEST!", redirectUrl: `${payLoad.openUrl}/login` }
        })
})


exports.createSessionCookies = functions.https.onCall((req, res) => {

    const idToken = req.idToken.toString();
    // Set session expiration to 5 days.
    const expiresIn = 60 * 60 * 24 * 5 * 1000;
    // Create the session cookie. This will also verify the ID token in the process.
    // The session cookie will have the same claims as the ID token.
    // To only allow session cookie setting on recent sign-in, auth_time in ID token
    // can be checked to ensure user was recently signed in before creating a session cookie.
    return admin
        .auth()
        .createSessionCookie(idToken, { expiresIn })
        .then(
            (sessionCookie) => {
                // Set cookie policy for session cookie.
                const options = { maxAge: expiresIn, httpOnly: true, secure: true };
                /*  res.cookie('session', sessionCookie, options);
                 res.end(JSON.stringify({ status: 'success', session: sessionCookie })); */
                return { status: 'success', session: sessionCookie }
            },
            (error) => {
                return 'UNAUTHORIZED REQUEST!';
            }
        );

})


exports.sessionLogout = functions.https.onCall((req, res) => {

    const sessionCookie = req.session || '';
    // res.clearCookie('session');
    admin
        .auth()
        .verifySessionCookie(sessionCookie)
        .then((decodedClaims) => {
            admin.auth().revokeRefreshTokens(decodedClaims.sub);
        })
        .then(() => {
            console.log('succss')
        })
        .catch((error) => {
            console.log(error)
        });

    // res.end(JSON.stringify({ status: 'success' }));
    return { status: 'success' }

});